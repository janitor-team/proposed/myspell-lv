myspell-lv (0.9.6-10) unstable; urgency=medium

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Fix day-of-week for changelog entries 0.7.2-1, 0.6.4-1.

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 12 Jan 2021 23:32:47 +0100

myspell-lv (0.9.6-9.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 17:16:27 +0100

myspell-lv (0.9.6-9) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/watch: Use https protocol

  [ Agustin Martin Domingo ]
  * d/control: Bump Standards-Version. No changes required.

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 15 Nov 2018 16:21:53 +0100

myspell-lv (0.9.6-8) unstable; urgency=medium

  * No longer ship myspell-lv from these sources. It has been superseded
    by transitional myspell-lv package from hunspell-lv 1.3.0-2 sources.
  * Make debian/copyright DEP-5.

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 06 Jun 2018 12:47:44 +0200

myspell-lv (0.9.6-7) unstable; urgency=medium

  * debian/rules::override_dh_auto_build: When building aspell cwl file,
    strip abbreviations with trailing dot and some words with
    parenthesized numbers (Closes: #897952).

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 08 May 2018 20:17:08 +0200

myspell-lv (0.9.6-6) unstable; urgency=medium

  * debian/control:
    - Update Vcs-Browser and Vcs-Git for salsa migration.
    - Bump Standards-Version. No changes required.
  * Bump debhelper compat level to 11.

 -- Agustin Martin Domingo <agmartin@debian.org>  Fri, 04 May 2018 16:31:36 +0200

myspell-lv (0.9.6-5) unstable; urgency=medium

  * debian/control::myspell-lv: Use Conflicts where needed.

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 22 Sep 2015 12:09:06 +0200

myspell-lv (0.9.6-4) unstable; urgency=medium

  * debian/control::myspell-lv:
    - Use Breaks rather than Conflicts for hunspell-lv.
    - Breaks also hyphen-lv.

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 21 Sep 2015 17:01:50 +0200

myspell-lv (0.9.6-3) unstable; urgency=medium

  * debian/rules: Do not pass timestamps to gzipped files.
  * debian/control: Bump Standards version. No changes required.

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 09 Jul 2015 20:33:11 +0200

myspell-lv (0.9.6-2) unstable; urgency=low

  * debian/watch: Search only for 0.* versions, 1.x are hunspell-only.
  * debian/control:
    - Add Vcs-Browser and Vcs-Git entries.
    - Bump Standards version. No changes required.
    - Modify to use dictionaries-common-dev (>= 1.22.0) features:
      - ${aspell:Depends}, ${hunspell:Depends}
      - Deal with remove files in debhelper snippet.
      - Build-dep on 1.23.3 for proper dir creation and cleaning.
    - Make myspell-lv conflict against future hunspell-lv.
    - Cosmetic changes

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 26 Jun 2014 17:09:24 +0200

myspell-lv (0.9.6-1) unstable; urgency=low

  * New upstream version.
  * debian/control: Bump Standards Version. No changes needed.

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 16 Sep 2013 16:34:55 +0200

myspell-lv (0.9.4-5) unstable; urgency=low

  * Remove Martin-Éric Racine from Uploaders (Closes: #659323).

 -- Agustin Martin Domingo <agmartin@debian.org>  Fri, 10 Feb 2012 14:50:52 +0100

myspell-lv (0.9.4-4) unstable; urgency=low

  * [aspell-lv.dirs]: deleted;
    + usr/lib/aspell and usr/share/aspell are created by [aspell-lv.install].
    + var/lib/aspell is handled by autobuildhash.
  * [aspell-lv.links]: deleted; handled by autobuildhash.
  * [myspell-lv.dirs]: deleted; obsolete content.
  * [myspell-lv.info-hunspell]: created rudimentary Hunspell support.
  * [debian/rules]: added installdeb-hunspell --package=myspell-$(DICT_LANG).
  * [debian/control]: updated myspell Provides for hunspell+hyphen.
  * Migrated to debhelper-7 build system:
    + Simplified [debian/rules] to use a single dh command with overrides.
    + Bumped the Build-Depends on debhelper to (>= 7.0.50~).
    + Dropped the Build-Depends on CDBS.
  * Migrated to source format 3.0 (quilt).
    + Build-Depends on dpkg-dev (>= 1.14.17~).
    + [debian/README.source]: deleted.

 -- Martin-Éric Racine <martin-eric.racine@iki.fi>  Mon, 24 Oct 2011 13:24:09 +0300

myspell-lv (0.9.4-3) unstable; urgency=low

  * Migrated to auto-compat for aspell-autobuildhash:
    + Requires dictionaries-common-dev (>= 1.11.2).
    + Removed obsolete debian/aspell-lv.{postinst,postrm} templates.
      Not needed when using autobuildhash.
    Thanks to Agustín Martín Domingo for the HOWTO.
  * Added dictionaries-common team's Agustín Martín Domingo to Uploaders.

 -- Martin-Éric Racine <martin-eric.racine@iki.fi>  Tue, 04 Oct 2011 14:03:19 +0300

myspell-lv (0.9.4-2) unstable; urgency=low

  * Migrated CDBS patch management from simple-patchsys.mk to patchsys-quilt.mk:
    + Adopted the short README.source from "Quilt for Debian Maintainers" as-is.
    + Added a Build-Depends on quilt.
  * Refreshed all patches using the .quiltrc from Chapter 3.1 of the NM Guide.
  * Lintian: W: removed myspell-lv.postinst and myspell-lv.postrm templates:
    Not used since LibreOffice/OpenOffice 3.0 and thus results in empty files.
  * Upgraded Standards-Version to 3.9.2 (no change required).

 -- Martin-Éric Racine <martin-eric.racine@iki.fi>  Sat, 01 Oct 2011 15:33:33 +0300

myspell-lv (0.9.4-1) unstable; urgency=low

  * New upstream release.
  * Updated my contact info in debian/[control|copyright|rules].

 -- Martin-Éric Racine <martin-eric.racine@iki.fi>  Sun, 29 May 2011 23:02:23 +0300

myspell-lv (0.9.3-2) unstable; urgency=low

  * Moving our changes to upstream to debian/patches/.
  * Upgraded Standards-Version to 3.9.1 (no change required).

 -- Martin-Éric Racine <q-funk@iki.fi>  Sun, 26 Sep 2010 22:30:39 +0300

myspell-lv (0.9.3-1) unstable; urgency=low

  * New upstream release.
  * Specified "1.0" in debian/source/format.

 -- Martin-Éric Racine <q-funk@iki.fi>  Tue, 21 Sep 2010 16:51:07 +0300

myspell-lv (0.9.1-2) unstable; urgency=high

  * Fixing an aspell transformation error (Closes: #591900)
  * Thanks to Martin-Eric Racine for the fixes.

 -- Aigars Mahinovs <aigarius@debian.org>  Sat, 07 Aug 2010 11:12:00 +0600

myspell-lv (0.9.1-1) unstable; urgency=low

  * New upstream version

 -- Aigars Mahinovs <aigarius@debian.org>  Sun, 01 Aug 2010 16:02:00 +0600

myspell-lv (0.7.3-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * move dict to /usr/share/hunspell and add compat symlinks from
    /usr/share/myspell/dicts (closes: #541943)
  * move hyphenation pattern to /usr/share/hyphen and add compat symlink
    from /usr/share/myspell/dicts (closes: #541900)

 -- Rene Engelhard <rene@debian.org>  Thu, 01 Oct 2009 23:44:52 +0200

myspell-lv (0.7.3-3) unstable; urgency=low

  * Fixed Lintian errors:
    W: command-with-path-in-maintainer-script
    W: maintainer-script-ignores-errors
    I: unused-override
  * Upgraded Standards-Version to 3.8.0 (no change required).

 -- Martin-Éric Racine <q-funk@iki.fi>  Mon, 04 May 2009 06:47:37 +0300

myspell-lv (0.7.3-2) unstable; urgency=low

  * Removed the Linda override, now that she is gone.

 -- Martin-Éric Racine <q-funk@iki.fi>  Tue, 01 Jul 2008 11:51:06 +0300

myspell-lv (0.7.3-1) unstable; urgency=low

  * New upstream release.
  * Cleaned up the upstream MySpell affix file with dos2unix, then
    merged Aigars' previous patch. Submitted the result to upstream.
  * Fixed the Lintian warning about incomplete copyright notice.
  * Upgraded Standards-Version to 3.7.3; no change required.
  * Added the hyphenation file to myspell-lv.install.

 -- Martin-Éric Racine <q-funk@iki.fi>  Fri, 29 Feb 2008 12:16:22 +0200

myspell-lv (0.7.2-4) unstable; urgency=low

  * Corrected aspell-lv.info-aspell to specify valid characters and
    make spelling work under Emacs again.

 -- Martin-Éric Racine <q-funk@iki.fi>  Sun, 26 Aug 2007 12:40:21 +0300

myspell-lv (0.7.2-3) unstable; urgency=low

  * Added debian/watch file.
    FIXME: it actually monitors for a larger tarball that includes the
    Latvian localisation for OpenOffice and the MySpell wordlist.

 -- Martin-Éric Racine <q-funk@iki.fi>  Fri,  3 Aug 2007 18:43:51 +0300

myspell-lv (0.7.2-2) unstable; urgency=low

  * Adding myself to Uploaders as agreed with Aigars.
  * Added Lintian override against usr-lib-in-arch-all to aspell-lv.
    (all Aspell dictionaries are packaged this way by design)
  * Patch to generate Aspell was merged in 0.7.2-1 (Closes: #405152).
  * Acknowledging previous NMU (Closes: #432450).

 -- Martin-Éric Racine <q-funk@iki.fi>  Fri,  3 Aug 2007 17:18:49 +0300

myspell-lv (0.7.2-1.1) unstable; urgency=low

  * Non-Maintainer Upload.
    - FTBFS: unmet build dep libmyspell-dev (Closes: #432450)
      Removing an unnecessary Build-Depends on MySpell tools fixes it.

 -- Martin-Éric Racine <q-funk@iki.fi>  Fri, 20 Jul 2007 23:10:13 +0300

myspell-lv (0.7.2-1) unstable; urgency=low

  * New upstream version

 -- Aigars Mahinovs <aigarius@debian.org>  Thu, 14 Jun 2007 11:52:40 +0100

myspell-lv (0.6.5.1-1.1) unstable; urgency=low

  * Non-Maintainer Upload:
    - Added aspell-lv target and corresponding package generation rules.
    - Upgraded to Standards-Version 3.7.2 (no change required).
    - Upgraded to CDBS.

 -- Martin-Éric Racine <q-funk@iki.fi>  Sun, 24 Dec 2006 05:50:50 +0200

myspell-lv (0.6.5.1-1) unstable; urgency=low

  * New upstream release
  * Description fixups (Closes: #213455)
  * Make the infoe file naming inline with others (Closes: #208752)
  * Add lv.{aff|dic} symlinks for Thunderbird (Closes: #373750)

 -- Aigars Mahinovs <aigarius@debian.org>  Tue, 11 Jul 2006 23:52:40 +0100

myspell-lv (0.6.4-1) unstable; urgency=low

  * New upstream release

 -- Aigars Mahinovs <aigarius@debian.org>  Tue, 12 Apr 2005 07:03:40 +0300

myspell-lv (0.6.3-1) unstable; urgency=low

  * New upstream release

 -- Aigars Mahinovs <aigarius@debian.org>  Sat, 26 Feb 2005 01:44:40 +0300

myspell-lv (0.5.5-1) unstable; urgency=low

  * Initial Release.

 -- Aigars Mahinovs <aigarius@debian.org>  Sun, 24 Aug 2003 01:44:40 +0300
